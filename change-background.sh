#!/bin/bash

# Function to print help information
print_help() {
	echo -e "";
	echo -e "Usage: ${0} -p <program> -b <backgrounds-directory> [OPTIONS]";
	echo -e "";
	echo -e "This script changes your background to random images contained within a specified directory";
	echo -e "The script can be executed one time only with -n, or the default is it will be executed periodically";
	echo -e "";
	echo -e "IMPORTANT: This script will search the backgrounds directory RECURSIVELY for all files. Also, this script DOES NOT validate that the files are valid images before attempting to display them. How big of a deal with is depends on the --program.";
	echo -e "";
	echo -e "Also note that it's common for file manager programs to have the responsibility of managing your background. For an unmodified setup, your --program may be your file manager with certain --args."
	echo -e "";
	echo -e "";
	echo -e "Important Arguments:";
	echo -e "  -p,  --program\t\tSpecify the program used to change the background image.";
	echo -e "  -a,  --args\t\t\tSpecify arguments for --program. Should be escaped with quotes.";
	echo -e "  -b,  --bgdir\t\t\tSpecify the directory containing the background images.";
	echo -e "";
	echo -e "Behavior Arguments:";
	echo -e "  -d,  --duration\t\tSpecify the amount of seconds to wait between loops (default 1 hour). Overrides -n/--noloop/--no-loop";
	echo -e "  -n,  --noloop, --no-loop\tCauses the script to exit immediately after changing the background. Overrides -d/--duration";
	echo -e "";
	echo -e "Other Arguments:";
	echo -e "  -h,  --help\t\t\tShows the help screen (this text)";
	echo -e "";
	echo -e "";
}



# Initialize some values to defaults
DURATION=3600;
PROGRAM_ARGS="";



# Handle flags
while [ "${1}" != "" ];
do
	case "${1}" in
		-p | --program)			PROGRAM=${2}; shift 2;;
		-b | --bgdir)			BGDIR=$(realpath ${2}); shift 2;;
		-a | --args)			PROGRAM_ARGS=${2}; shift 2;;
		-d | --duration)		DURATION=${2}; shift 2;;
		-n | --noloop | --no-loop)	DURATION=0; shift;;
		-h | --help )			print_help; exit;;
		* )				echo "Unknown option ${1}"; shift;;
	esac
done;



# Validate input
if [ -z ${PROGRAM} ];
then
	echo 'No progam specified! You must specify the program to execute to change the background image';
	exit;
fi;

if [ -z ${BGDIR} ];
then
	echo 'No backgrounds directory found';
	exit;
fi;



# Now actually do the logic
while : ;
do
	IMAGES=($(find ${BGDIR} -type f));
	IMAGES_COUNT=${#IMAGES[@]};

	${PROGRAM} ${PROGRAM_ARGS} "${IMAGES[$((RANDOM%IMAGES_COUNT))]}";

	if [ ${DURATION} == 0 ];
	then
		exit;
	fi;

	sleep ${DURATION};
done;
