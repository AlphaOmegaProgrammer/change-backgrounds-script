This is a simple bash script for changing your desktop background.

Read the help section with `./change-background.sh --help` for more information.
